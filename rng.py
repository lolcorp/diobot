import random
import asyncio
import requests
from discord import Game
from discord.ext.commands import Bot
from setuptools import setup
from discord.ext import commands
import discord



class RNG(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command(name='advice',
                    description="Get some wise advice",
                    aliases=[],
                    pass_context=False)
    async def advice(self, context):
        msg = context.message
        url = 'https://api.adviceslip.com/advice'
        response = requests.get(url)
        newAdvice = response.json()['slip']['advice']
        await msg.channel.send(newAdvice)




    @commands.command(name='8ball',
                    description="Answers a yes/no question.",
                    aliases=['eight_ball', 'eightball', '8-ball'],
                    pass_context=True)
    async def eight_ball(self, context):
        possible_responses = [
            'Nope',
            'Hell yeah boi',
            'Absolutely not',
            'Sure, why not',
            "I don't care",
            "WRYYYYYYY",
        ]
        msg = context.message
        await msg.channel.send(random.choice(possible_responses) + ", " + context.message.author.mention)






    @commands.command(name='coin',
                    description="DioBot does a coinflip for you",
                    aliases=['coinflip', 'coinaflip'],
                    pass_context=True)
    async def coin(self, context):
        msg = context.message
        numbers = [1, 2]
        result = random.choice(numbers)
        if result == 1:
            answer = "Heads"
        if result == 2:
            answer = "Tails"
        await msg.channel.send(answer)





    @commands.command(name='dice',
                    description="DioBot rolls a dice for you",
                    aliases=[],
                    pass_context=True)
    async def dice(self, context):
        msg = context.message
        numbers = ["1", "2", "3", "4", "5", "6"]
        result = random.choice(numbers)
        await msg.channel.send("You got a " + result)











    @commands.command(name='quote',
                    description="DioBot gives you a random quote",
                    aliases=['quotes'],
                    pass_context=True)
    async def quote(self, context):
        msg = context.message
        url = 'http://api.forismatic.com/api/1.0/?method=getQuote&key=457653&format=json&lang=en'
        response = requests.get(url)
        quote = response.json()['quoteText']
        author = response.json()['quoteAuthor']
        quoteSquare = discord.Embed(title = '"' + quote + '"', color = 0x3dc1ed)
        quoteSquare.set_footer(text = author)
        await msg.channel.send(embed = quoteSquare)








    @commands.command(name='funny',
                    description="DioBot tells you a funny quote",
                    aliases=['joke', 'lol'],
                    pass_context=True)
    async def funny(self, context):
        possible_responses = [
            'A cop pulled me over and told me "papers", so i said "scissors, i win!" and drove off',
            'An apple a day keeps anyone away, if you throw it hard enough',
            'I just wanted you to know that somebody cares. NOT ME, but somebody does',
            'The brocolli says "I look like a small tree", the mushroom says "I look like an umbrella", the walnut says "I look like a brain", and the banana says "Can we please change the subject?"',
            'I did not trip and fall. I attacked the floor and I believe I am winning',
            'Its simple to be wise. Just think of something stupid to say and then dont say it',
            'You know your driving is really terrible when your GPS says "After 300 feet, stop and let me out"',




        ]
        msg = context.message
        await msg.channel.send(random.choice(possible_responses))





    @commands.command(name='fact',
                    description="DioBot tells you a random fun fact",
                    aliases=['funfact', 'facts', 'trivia'],
                    pass_context=True)
    async def fact(self, context):
        possible_responses = [
            'Did you know: Banging you head against the wall burns 100 calories per hour',
            'Did you know: Pteronophobia is the fear of being tickled by feathers!',
            'Did you know: Nilly goats urinate on their own heads to smell more attractive to females',
            'Did you know: Human saliva has a boiling point three times that of regular water',
            'Did you know: During your lifetime, you will produce enough saliva to fill two swimming pools',
            'Did you know: If Pinocchio says “My Nose Will Grow Now”, it would cause a paradox',
            'Did you know: Polar bears can eat as many as 86 penguins in a single sitting. (If they lived in the same place)',
            'Did you know: Movie trailers were originally shown after the movie, which is why they were called “trailers”',
            'Did you know: Heart attacks are more likely to happen on a Monday',
            'Did you know: An eagle can kill a young deer and fly away with it',
            'Did you know: Rem is best girl!',
            'Did you know: If you consistently fart for 6 years & 9 months, enough gas is produced to create the energy of an atomic bomb!',
            'Did you know: In 2015, more people were killed from injuries caused by taking a selfie than by shark attacks',
            'Did you know: There is a species of spider called the Hobo Spider',
            'Did you know: A "jiffy" is an actual unit of time: 1/100th of a second',
            'Did you know: Leonardo Da Vinci invented scissors',
            'Did you know: An average of 100 people choke to death on ballpoint pens every year',
            'Did you know: Antarctica is the only continent without reptiles or snakes.',
            'Did you know: The star Antares is 60,000 times larger than our sun. If our sun were the size of a softball, the star Antares would be as large as a house',
            'Did you know: In space, astronauts cannot cry because there is no gravity so tears cant flow',
            'Did you know: Most lipstick contains fish scales',
            'Did you know: The cigarette lighter was invented before the match',
            'Did you know: A category three hurricane releases more energy in ten minutes that all the worlds nuclear weapons combined',
            'Did you know: February 1865 is the only month in recorded history not to have a full moon',
            'Did you know: Nutmeg is extremely poisonous if injected intravenously',
            'Did you know: According to Genesis 1:20-22, the chicken came before the egg',

        ]
        msg = context.message
        await msg.channel.send(random.choice(possible_responses))




def setup(client):
    client.add_cog(RNG(client))
