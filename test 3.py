import random

data = [
    ["Dio", "Part 3", "The World"],
    ["Jotaro", "Part 3", "Star Platinum"],
    ["Josuke", "Part 4", "Crazy Diamond"],
    ["Joseph", "Part 3", "Hermit Purple"],
    ["Giorno", "Part 5", "Gold Experience"],
    ["Jolyne", "Part 6", "Stone Free"],
    ["Johnny", "Part 7", "Tusk"],
    ["Gappy", "Part 8", "Soft & Wet"],
]

#0 = Name
#1 = Part
#2 = Stand

character = random.choice(data)

class Character:
    def __init__(self, name, part, stand):
        self.name = name
        self.part = part
        self.stand = stand

characters = [
    Character("Dio", "3", "The World"),
    Character("Jotaro", "3", "Star Platinum"),
    Character("Josuke", "4", "Crazy Diamond"),
    Character("Giorno", "5", "Gold Experience"),
]

char = random.choice(characters)
sentence = "The character is " + char.name + " from Part " + char.part + " and their stand is 「" + char.stand + "」"

print(sentence)
