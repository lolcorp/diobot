import random
import asyncio
import requests
from discord import Game
from discord.ext.commands import Bot
from setuptools import setup
from discord.ext import commands
import discord



class Weeb(commands.Cog):
    def __init__(self, client):
        self.client = client

    @commands.command(name='anime',
                    description="Search for an anime",
                    aliases=[],
                    pass_context=True)
    async def anime(self, context):
        msg = context.message
        if len(context.message.content) == 6:
            await msg.channel.send("You forgot to search for something")
            return
        anime = context.message.content[7:]
        format_anime = anime.replace(" ", "%20")
        url = 'https://kitsu.io/api/edge/anime?filter[text]=' + format_anime
        response = requests.get(url)
        try:
            data = response.json()['data'][0]
        except:
            await msg.channel.send("Sorry, but i can't seem to find this anime")
        try:
            title = data['attributes']['titles']['en_us']
        except:
            title = data['attributes']['titles']['en_jp']
        try:
            image = data['attributes']['posterImage']['medium']
        except:
            pass
        try:
            episodes = data['attributes']['episodeCount']
            if episodes == None:
                episodes = "No info found"
        except:
            episodes = "No info found"
        try:
            ageRating = data['attributes']['ageRating']
            ageRatingGuide = data['attributes']['ageRatingGuide']
            ageTogether = ageRating + " - " + ageRatingGuide
        except:
            ageTogether = "No age rating found"
        try:
            desc = data['attributes']['synopsis']
        except:
            desc = "No summary found"
        id = data['id']
        try:
            rating = data['attributes']['averageRating']
        except:
            rating = "No rating found"
        try:
            status = data['attributes']['status']
            if status == "current":
                status = "Currently airing"
            elif status == "finished":
                status = "Finished"
        except:
            status = "No status found"
        link = "https://kitsu.io/anime/" + id
        infoSquare = discord.Embed(title = title, url = link, description = desc, color = 0x2e51a2)
        infoSquare.set_thumbnail(url = image)
        infoSquare.add_field(name = "Episodes:", value = episodes, inline = False)
        infoSquare.add_field(name= "Status:", value = status, inline = False)
        infoSquare.add_field(name = "Rating:", value = rating, inline = False)
        infoSquare.add_field(name = "Age rating:", value = ageTogether, inline = False)
        await msg.channel.send(embed = infoSquare)



    @commands.command(name='kitsu',
                      description="get a kitsu profile",
                      aliases=[],
                      pass_context=True)
    async def kitsu(self, context):
        msg = context.message
        user = context.message.content[7:]
        url = "https://kitsu.io/api/edge/users?filter[name]=" + user
        response = requests.get(url)
        try:
            data = response.json()['data'][0]
        except:
            await msg.channel.send("Sorry, but i can't seem to find this user")
        id = data['id']
        try:
            loverurl = data['relationships']['waifu']['links']['related']
        except:
            try:
                loverurl = data['relationships']['husbando']['related']
            except:
                pass
        name = data['attributes']['name']
        try:
            about = data['attributes']['about']
            if about == "":
                about = "No info"
        except:
            about = "No info"
        try:
            avatar = data['attributes']['avatar']['tiny']
        except:
            pass
        try:
            lover = data['attributes']['waifuOrHusbando']
        except:
            pass
        link = "https://kitsu.io/users/" + id
        infoSquare = discord.Embed(title = "About:", description = about, color = 0x2e51a2)
        infoSquare.set_author(name = name, url = link, icon_url = avatar)
        loverResponse = requests.get(loverurl)
        try:
            loverData = loverResponse.json()['data']
        except:
            pass
        try:
            loverName = loverData['attributes']['canonicalName']
        except:
            pass
        try:
            loverImage = loverData['attributes']['image']['original']
        except:
            pass
        try:
            loverInfo = loverData['attributes']['description']
            if loverInfo == "":
                loverInfo = "Couldn't find any info"
        except:
            loverInfo = "Couldn't find any info"
        try:
            infoSquare.add_field(name = lover + ": " + loverName, value = loverInfo + " on " + loverName)
        except:
            pass
        try:
            infoSquare.set_thumbnail(url = loverImage)
        except:
            pass
        await msg.channel.send(embed = infoSquare)


    @commands.command(name='dio',
                    description="DioBot makes a classic Dio sound",
                    aliases=[],
                    pass_context=True)
    async def dio(self, context):
        msg = context.message
        quotes = [
            'https://www.youtube.com/watch?v=HaGkk60kcjQ', #KONO DIO DA [0]
            'https://www.youtube.com/watch?v=5_KwV-MAMQ8', #MUDA MUDA MUDA [1]
            'https://www.youtube.com/watch?v=t1y3QOIRsYs', #ROAD ROLLER DA [2]
            'https://www.youtube.com/watch?v=kSkeqYkd-ng', #ZA WARUDO [3]
            'https://www.youtube.com/watch?v=uVUVLvHcwF0', #WRYYYYY [4]


        ]
        url = (random.choice(quotes))
        gifs = [
            'https://thumbs.gfycat.com/AnchoredTornAzurewingedmagpie-small.gif', #KONO DIO DA [0]
            'https://thumbs.gfycat.com/SlimSeriousBlackwidowspider-size_restricted.gif', #MUDA MUDA MUDA [1]
            'https://thumbs.gfycat.com/SpitefulArcticAmurratsnake-size_restricted.gif', #ROAD ROLLER DA [2]
            'https://i.imgur.com/KzE0XWk.gif', #ZA WARUDO [3]
            'https://media1.tenor.com/images/e6ff3cbf8d62f2c3adc50eb11dbaf4ef/tenor.gif?itemid=12098414', #WRYYYY [4]
        ]
        if url == quotes[0]:
            link = gifs[0]
            picture = discord.Embed(description = "**BUT IT WAS ME!**", color = 0xf1c40f)
            picture.set_image(url = link)
            await msg.channel.send(embed = picture)
            return
        if url == quotes[1]:
            link = gifs[1]
            picture = discord.Embed(description = "**IT'S USELESS!**", color = 0xf1c40f)
            picture.set_image(url = link)
            await msg.channel.send(embed = picture)
            return
        if url == quotes[2]:
            link = gifs[2]
            picture = discord.Embed(description = "**ROAD ROLLER DA!**", color = 0xf1c40f)
            picture.set_image(url = link)
            await msg.channel.send(embed = picture)
            return
        if url == quotes[3]:
            link = gifs[3]
            picture = discord.Embed(description = "**ZA WARUDO!**", color = 0xf1c40f)
            picture.set_image(url = link)
            await msg.channel.send(embed = picture)
            return
        if url == quotes[4]:
            link = gifs[4]
            picture = discord.Embed(description = "**WRYYYYYYY**", color = 0xf1c40f)
            picture.set_image(url = link)
            await msg.channel.send(embed = picture)
            return




    @commands.command(name='rem',
                    description="Rem is best girl",
                    aliases=['Rem', 'REM'],
                    pass_context=True)
    async def rem(self, context):
        possible_responses = [
            'http://i0.kym-cdn.com/photos/images/newsfeed/001/173/128/d74.png',
            'https://pre00.deviantart.net/087f/th/pre/i/2017/082/c/f/re_zero_rem_kawaii_wallpaper_by_aighix-db3a512.png',
            'https://i.redd.it/v7vlp9hxj6az.png',
            'https://i.pinimg.com/originals/d3/28/24/d328245563f006159219013590f75bfd.jpg',
            'https://pics.prcm.jp/6dad9f270c45d/77569897/jpeg/77569897.jpeg'

        ]
        msg = context.message
        link = random.choice(possible_responses)
        picture = discord.Embed(title = "Rem is best girl!", color = 0x6f9ded)
        picture.set_image(url = link)
        await msg.channel.send(embed = picture)



    @commands.command(name='ratewaifu',
                    description="Rates your waifu",
                    aliases=[],
                    pass_context=True)
    async def ratewaifu(self, context):
        msg = context.message
        numbers = list(range(1, 101))
        rating = random.choice(numbers)
        if rating == 100:
            await msg.channel.send(context.message.content[11:] + " is totally a 100/100, congratulations!")
        elif rating == 1:
            await msg.channel.send(context.message.content[11:] + " is a 1/100, can't be more trash than that.")
        else:
            await msg.channel.send("I give " + context.message.content[11:] + " a " + str(rating) + "/100")



def setup(client):
    client.add_cog(Weeb(client))
