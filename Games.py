import random
import asyncio
import requests
from discord import Game
from discord.ext.commands import Bot
from setuptools import setup
from discord.ext import commands
import discord

def check(author):
    print("test 1")
    def inner_check(message):
        print("test 2")
        if message.author == author:
            print("test 3")
            return True
        else:
            return False
    return inner_check

class Character():
    def __init__(self, name, answer, series, url):
        self.name = name
        self. answer = answer
        self.series = series
        self.url = url



class Games(commands.Cog):
    def __init__(self, client):
        self.client = client



    @commands.command(name='guess_anime',
                    description="DioBot gives you an anime character you have to guess",
                    aliases=[],
                    pass_context=True)
    async def guess_anime(self, context):
        characters = [
            Character('Saitama', ['saitama'], "One Punch Man", 'https://cdn.myanimelist.net/images/characters/8/346084.jpg'), #Saitama [0]
            Character('Okabe Rintaro', ["okabe rintaro", "okabe", "okarin"], "Steins;Gate", 'https://cdn.myanimelist.net/images/characters/15/310991.jpg'), #Okabe Rintarou [1]
            Character('Miia', ['miia'], "Monster Musume", 'https://cdn.myanimelist.net/images/characters/3/287359.jpg'), #Miia [2]
            Character('Rem', ['rem', 'remrin'], "Re:Zero", 'https://cdn.myanimelist.net/images/characters/3/311910.jpg'), #Rem [3]
            Character('Roy Mustang', ['roy mustang', 'roy', 'mustang'], "Fullmetal Alchemist", 'https://cdn.myanimelist.net/images/characters/2/35643.jpg'), #Roy Mustang [4]
            Character('Ryuk', ['ryuk'], "Death Note", 'https://cdn.myanimelist.net/images/characters/3/83192.jpg'), #Ryuk [5]
            Character('Subaru Natsuki', ['subaru natsuki', 'natsuki subaru', 'subaru'], "Re:Zero", 'https://cdn.myanimelist.net/images/characters/9/307012.jpg'), #Subaru Natsuki [6]
            Character('Midoriya Izuku', ['midoriya izuku', 'izuku midoriya', 'midoriya', 'deku'], "My Hero Academia", 'https://cdn.myanimelist.net/images/characters/10/314555.jpg'), #Midoriya Izuku [7]
            Character('Genos', ['genos'], "One Punch Man", 'https://cdn.myanimelist.net/images/characters/9/297329.jpg'), #Genos [8]
            Character('Eren Jaeger', ['eren jaeger', 'eren', 'eren yaeger'], "Attack on Titan", 'https://cdn.myanimelist.net/images/characters/10/216895.jpg'), #Eren Jaeger [9]
            Character('Edward Elric', ['edward elric', 'edward', 'ed'], "Fullmetal Alchemist", 'https://cdn.myanimelist.net/images/characters/10/274047.jpg'), #Edward Elric [10]
            Character('Alphonse Elric', ['alphonse elric', 'alphonse', 'al'], "Fullmetal Alchemist", 'https://cdn.myanimelist.net/images/characters/6/86610.jpg'), #Alphonse Elric [11]
            Character('Mikasa Ackerman', ['mikasa ackerman', 'mikasa'], "Attack on Titan", 'https://cdn.myanimelist.net/images/characters/6/370448.jpg'), #Mikasa Ackerman [12]
            Character('Light Yagami', ['light yagami', 'yagami light', 'light', 'kira'], "Death Note", 'https://cdn.myanimelist.net/images/characters/12/63809.jpg'), #Light Yagami [13]
            Character('Mumen Rider', ['mumen rider'], "One Punch Man", 'https://cdn.myanimelist.net/images/characters/6/290361.jpg'), #Mumen Rider [14]
            Character('Kujo Jotaro', ['kujo jotaro', 'jotaro kujo', 'jotaro', 'jojo'], "JoJo's Bizarre Adventure: Stardust Crusaders", 'https://cdn.myanimelist.net/images/characters/16/342554.jpg'), #Jotaro Kujo [15]
            Character('Emilia', ['emilia'], "Re:Zero", 'https://cdn.myanimelist.net/images/characters/7/302100.jpg'), #Emilia [16]
            Character('Makise Kurisu', ['makise kurisu', 'kurisu makise', 'kurisu', 'christina', 'makise'], "Steins;Gate", 'https://cdn.myanimelist.net/images/characters/10/310993.jpg'), #Kurisu Makise [17]
            Character('Dio Brando', ['dio brando', 'dio'], "JoJo's Bizarre Adventure", 'https://cdn.myanimelist.net/images/characters/13/328191.jpg'), #Dio Brando [18]
            Character('Higashikata Josuke', ['higashikata josuke', 'josuke higashikata', 'josuke', 'jojo'], "JoJo's Bizarre Adventure: Diamond is Unbreakable", 'https://cdn.myanimelist.net/images/characters/9/311632.jpg'), #Higashikata Josuke [19]
            Character('Yoshikage Kira', ['yoshikage kira', 'kira yoshikage', 'kira'], "JoJo's Bizarre Adventure: Diamond is Unbreakable", 'https://cdn.myanimelist.net/images/characters/13/327061.jpg'), #Yoshikage Kira [20]
            Character('Arataka Reigen', ['arataka reigen', 'reigen arataka', 'reigen'], "Mob Psycho 100", 'https://cdn.myanimelist.net/images/characters/6/339134.jpg'),
            Character('Giorno Giovanna', ['giorno giovanna', 'giorno', 'giogio'], "JoJo's Bizarre Adventure: Golden Wind", 'https://cdn.myanimelist.net/images/characters/7/364980.jpg'),
            Character('Touka Kirishima', ['touka kirishima', 'kirishima touka', 'touka'], "Tokyo Ghoul", 'https://cdn.myanimelist.net/images/characters/15/269993.jpg'),
            Character('Kaneki Ken', ['kaneki ken', 'ken kaneki', 'kaneki'], "Tokyo Ghoul", "https://cdn.myanimelist.net/images/characters/15/307255.jpg"),
        ]

        #char = characters[-1]
        msg = context.message
        char = random.choice(characters)
        winner = 'none'
        picture = discord.Embed(title = "Guess the character!", color = 0xf1c40f)
        picture.set_image(url = char.url)
        picture.set_footer(text = "Use either -series or -letter for clues")
        await msg.channel.send(embed = picture)





        while winner == 'none':
            guess = await self.client.wait_for("message", check=check(msg.author))
            if guess.content.startswith("-") == False:
                text = guess.content.lower()

                if text in char.answer:
                    await msg.channel.send("That's right " + guess.author.mention)
                    return
                else:
                    await msg.channel.send("Wrong, the character was " + char.name)
                    return

            if guess.author != self.client.user and guess.content.startswith("-") == True:
                if guess.content == "-series":
                    await msg.channel.send('This character is from the anime "' + char.series + '"')
                if guess.content == "-letter":
                    await msg.channel.send("The first letter is: " + char.name[0])






    @commands.command(name="hangman",
                      description="Play hangman with DioBot",
                      aliases=[],
                      pass_context=True)
    async def hangman(self, context):
        msg = context.message
        await msg.channel.send("Type -end to end the game at any time")
        play_again = True

        while play_again:


            words = ["star platinum", "crazy diamond", "hermit purple", "silver chariot",
                     "subaru natsuki", "kujo jotaro", "higashikata josuke", "okabe rintaro",
                     "killer queen", "the world", "kira yoshikage", "dio brando", "joseph joestar",
                     "emerald splash", "kakyoin noriaki", "hierophant green",
                     "jonathan joestar", "gold experience", "giorno giovanna", "jolyne cujoh",
                     "king crimson", "stone free", "johnny joestar", "gyro zeppeli", "caesar zeppeli",
                     "funny valentine", "jean pierre polnareff", "robert e o speedwagon",
                     ]

            #chosen_word = words[-1].upper()
            chosen_word = random.choice(words).upper()
            player_guess = None
            guessed_letters = []
            word_guessed = []
            for letter in chosen_word:
                word_guessed.append("-")
            joined_word = None
            attempts = 10


            while (attempts != 0 and "-" in word_guessed):
                for letter in range(len(chosen_word)):
                    if " " == chosen_word[letter]:
                        word_guessed[letter] = " "
                joined_word = "".join(word_guessed)
                word_square = discord.Embed(title = joined_word, color = 0xf1c40f)
                word_square.set_footer(text = str(attempts) + " more attempts!")
                await msg.channel.send(embed = word_square)


                player_guess = await self.client.wait_for("message", check=check(msg.author))
                player_guess = player_guess.content.upper()
                if player_guess == "-END":
                    await msg.channel.send("Understood, ending game")
                    play_again = False
                    break;

                if not player_guess.isalpha(): # check the input is a letter. Also checks an input has been made.
                    await msg.channel.send("That is not a letter. Please try again.")
                    continue
                elif len(player_guess) > 1: # check the input is only one letter
                    await msg.channel.send("That is more than one letter. Please try again.")
                    continue
                elif player_guess in guessed_letters: # check that the letter hasn't been guessed already
                    await msg.channel.send("You have already guessed that letter. Please try again.")
                    continue
                else:
                    pass


                guessed_letters.append(player_guess)

                for letter in range(len(chosen_word)):
                    if player_guess == chosen_word[letter]:
                        word_guessed[letter] = player_guess

                if player_guess not in chosen_word:
                    attempts -= 1

            if "-" not in word_guessed:
                word_square = discord.Embed(title = "Congratulations! **" + chosen_word + "** was the word", color = 0xf1c40f)
                word_square.set_footer(text = "Use -hangman to play again!")
                await msg.channel.send(embed = word_square)
                play_again = False



            else:
                word_square = discord.Embed(title = "Unlucky! The word was **" + chosen_word + "**", color = 0xf1c40f)
                word_square.set_footer(text = "Use -hangman to play again!")
                await msg.channel.send(embed = word_square)

                play_again = False



def setup(client):
    client.add_cog(Games(client))
