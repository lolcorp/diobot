import random
import asyncio
import requests
from discord import Game
from discord.ext.commands import Bot
from setuptools import setup
from discord.ext import commands
import discord



class Reactions(commands.Cog):
    def __init__(self, client):
        self.client = client




    @commands.command(name='kill',
                    description="Use this command to kill someone",
                    aliases=['murder'],
                    pass_context=True)
    async def kill(self, context):
        murder = [
            'https://yukimoongfx.files.wordpress.com/2016/07/re-zero_tumblr_o78o1w01g21qet0cro1_540.gif?w=640',
            'http://gifimage.net/wp-content/uploads/2017/07/higurashi-gif-4.gif',
            'https://78.media.tumblr.com/3a388cb8acb23f8fd289c96f7a51fc00/tumblr_o9wibjennl1tvso1qo1_500.gif',
            'http://i.imgur.com/BKwZe79.gif',
            'https://pa1.narvii.com/5742/bf5198e1429317217c8c6e79520f7da0b7cc2a32_hq.gif',
            'https://i.makeagif.com/media/11-08-2015/Z3X7Dj.gif',
        ]
        suicide = [
            'https://media.giphy.com/media/WsWJZcJoxmLUk/giphy.gif',
            'https://pa1.narvii.com/5773/6b54cae1dc28b51d3f5227be910475811254a5f1_hq.gif',
            'http://gifimage.net/wp-content/uploads/2017/07/anime-suicide-gif-8.gif',
        ]
        link = random.choice(murder)
        msg = context.message
        if not msg.mentions:
            await msg.channel.send("Tag someone to kill!")
            return
        if msg.author == context.message.mentions[0]:
            url = random.choice(suicide)
            picture = discord.Embed(description = context.message.author.mention + " comitted suicide", color = 0x992d22)
            picture.set_image(url = url)
            await msg.channel.send(embed = picture)
            return
        victim = context.message.mentions[0]
        if victim.id == "218090761885974530":
            await msg.channel.send("I won't let you kill my creator")
            return
        if victim == self.client.user:
            await msg.channel.send("You can't kill me, I'm unkillable!")
            return
        picture = discord.Embed(description = victim.mention + " was brutally murdered by " + context.message.author.mention, color = 0x992d22)
        picture.set_image(url = link)
        await msg.channel.send(embed = picture)





    @commands.command(name='thumbsup',
                    description="Give someone a thumbs up",
                    aliases=[],
                    pass_context=True)
    async def thumbsup(self, context):
        possible_responses = [
            'https://78.media.tumblr.com/e6a4c06b5b9cd713aaa12f282327c8c8/tumblr_inline_o1kgbrVDqu1s0yiss_500.gif',
            'https://media.tenor.com/images/3c90d713e87d557a8e761ab4b259a403/tenor.gif',
            'https://thumbs.gfycat.com/GrandScratchyCicada-size_restricted.gif',
            'https://m.popkey.co/161794/QlrwN.gif',
            'http://gifimage.net/wp-content/uploads/2017/07/anime-thumbs-up-gif-6.gif',



        ]
        link = random.choice(possible_responses)
        msg = context.message
        if not context.message.mentions:
            await msg.channel.send("Tag someone who deserves a thumbs up!")
            return
        if context.message.author == context.message.mentions[0]:
            await msg.channel.send("It's pretty lame to give yourself a thumbs up")
            return
        victim = context.message.mentions[0]
        picture = discord.Embed(description = context.message.author.mention + " gave a thumbs up to " + victim.mention)
        picture.set_image(url = link)
        await msg.channel.send(embed = picture)






    @commands.command(name='fight',
                    description="Engage a user in an anime battle",
                    aliases=['battle'],
                    pass_context=True)
    async def fight(self, context):
        possible_responses = [
            'https://i.imgur.com/jIArrsS.gif',
            'https://media.giphy.com/media/NK1x68ZH6KojS/giphy.gif',
            'https://78.media.tumblr.com/4362aa42db65b4ac15ab975f2cd6a867/tumblr_nqmaxz1QWn1twyezqo1_400.gif',
            'http://pin.anime.com/wp-content/uploads/2015/07/as-most-of-you-know-this-is-samurai-champloo-it-is-one-of-my-favorite-animes-i-am-asking-for-anime-r-14370089238kn4g.gif',
            'http://gifimage.net/wp-content/uploads/2017/09/anime-fighting-gif-8.gif',
        ]
        diobot = [
            'https://media1.tenor.com/images/1b29129d42435f4eea6038998802b04c/tenor.gif?itemid=5371460',

        ]
        link = random.choice(possible_responses)
        msg = context.message
        url = random.choice(diobot)
        if not context.message.mentions:
            await msg.channel.send("Tag someone you want to fight!")
            return
        if context.message.author == context.message.mentions[0]:
            await msg.channel.send("You can't fight yourself!")
            return
        victim = context.message.mentions[0]
        if victim == self.client.user:
            picture = discord.Embed(description = "Oh you're approaching me? " + context.message.author.mention)
            picture.set_image(url = url)
            await msg.channel.send(embed = picture)
            return
        picture = discord.Embed(description = context.message.author.mention + " engaged " + victim.mention + " in combat!")
        picture.set_image(url = link)
        await msg.channel.send(embed = picture)






    @commands.command(name='dab',
                    description="DioBot dabs on them haters",
                    aliases=['haters'],
                    pass_context=True)
    async def dab(self, context):
        possible_responses = [
            'https://media0.giphy.com/media/A4R8sdUG7G9TG/giphy.gif',
            'https://media1.tenor.com/images/9b2147e6ad5a8c7f0ae0f39d37230a56/tenor.gif?itemid=9672617',
            'https://thumbs.gfycat.com/TotalBleakBarasinga-size_restricted.gif',
            'https://media.giphy.com/media/3h5VTO6F1i2gU/giphy.gif',
            'https://m.popkey.co/5c890b/V01lJ.gif',
            'https://media4.giphy.com/media/3oz8xPyx3qgq5jAmMo/giphy-downsized.gif',
            'http://gifimage.net/wp-content/uploads/2017/09/bill-gates-dab-gif-5.gif',
            'https://media0.giphy.com/media/3oz8xzgGdsIpE8kPBu/200w.gif',
            'https://orig00.deviantart.net/bde4/f/2017/174/9/8/sp_animated_gif___super_best_friends__the_dab__by_flip_reaper_z-dbds44v.gif',
            'https://i.imgur.com/9kaetUj.gif',



        ]
        link = random.choice(possible_responses)
        msg = context.message
        picture = discord.Embed(description = context.message.author.mention + " dabs on them haters!")
        picture.set_image(url = link)
        await msg.channel.send(embed = picture)






    @commands.command(name='yes',
                    description="Say yes with a JoJo reference",
                    aliases=[],
                    pass_context=True)
    async def yes(self, context):
        possible_responses = [
            'https://i.imgur.com/Qgl3Q2K.gif',




        ]
        link = random.choice(possible_responses)
        msg = context.message
        picture = discord.Embed(description = context.message.author.mention + " agrees!", color = 0x7b108b)
        picture.set_image(url = link)
        await msg.channel.send(embed = picture)



    @commands.command(name='no',
                    description="Say no with a JoJo reference",
                    aliases=[],
                    pass_context=True)
    async def no(self, context):
        possible_responses = [
            'https://thumbs.gfycat.com/MerryHeftyGavial-size_restricted.gif',




        ]
        link = random.choice(possible_responses)
        msg = context.message
        picture = discord.Embed(description = context.message.author.mention + " disagrees!", color = 0x7b108b)
        picture.set_image(url = link)
        await msg.channel.send(embed = picture)





def setup(client):
    client.add_cog(Reactions(client))
