import random
import asyncio
import requests
from discord import Game
from discord.ext.commands import Bot
from setuptools import setup
from discord.ext import commands
import discord



class Music(commands.Cog):
    def __init__(self, client):
        self.client = client





    @commands.command(name='play',
                    description="Play music",
                    aliases=[],
                    pass_context=True)
    async def play(self, ctx):
        msg = ctx.message
        url = ctx.message.content[6:]
        author = ctx.message.author
        try:
            voice_channel = ctx.author.voice.channel
        except:
            await msg.channel.send("Please join a voice channel before using this command")
            return
        voice = get(self.client.voice_clients, guild=ctx.guild)

        if voice and voice.is_connected():
            await voice.move_to(voice_channel)
        else:
            voice = await voice_channel.connect()
        player = await vc.create_ytdl_player(url, ytdl_options={'default_search': 'auto'})
        player.start()
        print(url)








    @commands.command(name='op',
                    description="BoiBot plays a random anime opening",
                    aliases=['opening', 'intro'],
                    pass_context=True)
    async def op(self, ctx):
        openings = [
            ['https://www.youtube.com/watch?v=_TUTJ0klnKk', 'The Hero!! (One Punch Man)'],
            ['https://www.youtube.com/watch?v=EV4Tc014Hek', 'Redo (Re:Zero)'],
            ['https://www.youtube.com/watch?v=SBQprWeOx8g', 'Hacking to the Gate (Steins;Gate)'],
            ['https://www.youtube.com/watch?v=XMXgHfHxKVM', 'Guren no Yumiya (Attack on Titan)'],
            ['https://www.youtube.com/watch?v=09r-78dfrvc', 'Kuusou Mesorogiwi (Mirai Nikki)'],
            ['https://www.youtube.com/watch?v=2uq34TeWEdQ', 'Again (Fullmetal Alchemist Brotherhood)'],
            ['https://www.youtube.com/watch?v=8QE9cmfxx4s', 'The World (Death Note)'],
            ['https://www.youtube.com/watch?v=6wDSFQFKSo0', "Bloody Stream (JoJo's Bizarre Adventure)"],
            ['https://www.youtube.com/watch?v=-77UEct0cZM', "The Day (Boku no Hero Academia)"],
            ['https://www.youtube.com/watch?v=8m31lxggdHg', "Stand Proud (JoJo's Bizarre Adventure: Stardust Crusaders)"],
            ['https://www.youtube.com/watch?v=OcM5Kgjei-Y', "End of the World (JoJo's Bizarre Adventure: Stardust Crusaders)"],
            ['https://www.youtube.com/watch?v=Rs0N_Lq6Gmo', "Crazy Noisy Bizarre Town (JoJo's Bizarre Adventure: Diamond is Unbreakable)"],
            ['https://www.youtube.com/watch?v=R6zN_JSGk1U', "Chase (JoJo's Bizarre Adventure: Diamond is Unbreakable)"],
            ['https://www.youtube.com/watch?v=oBiVN5T7dv8', "Great Days (JoJo's Bizarre Adventure: Diamond is Unbreakable)"],
            ['https://www.youtube.com/watch?v=1xJbdY9B3A8&t=3s', "Fatima (Steins;Gate 0)"],
            ['https://www.youtube.com/watch?v=ucXk4i2vfpg', "Paradisus-Paradoxum (Re:Zero)"]


        ]
        opening = (random.choice(openings))
        author = ctx.message.author
        voice_channel = author.voice_channel
        if author.voice_channel is None:
            await self.client.say("Please join a voice channel before using this command")
            return
        vc = await self.client.join_voice_channel(voice_channel)

        if url == openings[0]:
            await self.client.say("**Now playing:** " + openings[1])

        def leave():
            stop = vc.disconnect()
            fut = asyncio.run_coroutine_threadsafe(stop, vc.loop)
            fut.result()

        player = await vc.create_ytdl_player(openings[0], after = leave)
        player.start()





    @commands.command(name='stop',
                    description="BoiBot stops the music currently playing",
                    aliases=[],
                    pass_context=True)
    async def stop(self, ctx):
        for x in self.client.voice_clients:
            if(x.server == ctx.message.server):
                await self.client.say("Disconnecting...")
                return await x.disconnect()

        return await self.client.say("There is no music playing!")



def setup(client):
    client.add_cog(Music(client))
